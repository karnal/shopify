package tech.jborn.shopify.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jborn.shopify.db.repository.ProductRepository;
import tech.jborn.shopify.response.ProductResponse;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ProductsController {
    private final ProductRepository productRepository;

    @GetMapping("/products")
    public List<ProductResponse> getProducts() {
        return productRepository.findAll()
                                .stream()
                                .map(product -> new ProductResponse().setProductId(product.getId())
                                                                     .setName(product.getName())
                                                                     .setPrice(product.getPrice()))
                                .collect(toList());
    }
}
