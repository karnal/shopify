package tech.jborn.shopify.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.jborn.shopify.db.entity.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByIdIn(List<Long> ids);
    Product findByName(String name);
}
