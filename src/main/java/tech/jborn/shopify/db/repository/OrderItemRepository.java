package tech.jborn.shopify.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.jborn.shopify.db.entity.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
}
