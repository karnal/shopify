package tech.jborn.shopify.request;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class OrderRequest {
    @Valid
    @NotEmpty
    private List<OrderItemRequest> items;
}
