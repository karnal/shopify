package tech.jborn.shopify.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class ProductResponse {
    private Long productId;
    private String name;
    private BigDecimal price;
}
