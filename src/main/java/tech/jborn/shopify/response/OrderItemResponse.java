package tech.jborn.shopify.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderItemResponse {
    private Long orderItemId;
    private String productName;
    private BigDecimal amount;
    private BigDecimal price;
}
