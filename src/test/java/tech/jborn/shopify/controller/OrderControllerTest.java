package tech.jborn.shopify.controller;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import tech.jborn.shopify.AbstractTest;
import tech.jborn.shopify.db.entity.Order;
import tech.jborn.shopify.db.entity.OrderItem;
import tech.jborn.shopify.db.entity.Product;
import tech.jborn.shopify.db.repository.OrderRepository;
import tech.jborn.shopify.db.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.List;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

class OrderControllerTest extends AbstractTest {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ProductRepository productRepository;

    @Test
    void createOrder() {
        Product latte = productRepository.findByName("latte");

        Integer orderId = given()
                    .mockMvc(mockMvc)
                    .contentType(ContentType.JSON)
                    .body("{\n" +
                            "  \"items\": [\n" +
                            "    {\n" +
                            "      \"productId\": " + latte.getId() + ",\n" +
                            "      \"amount\": 2\n" +
                            "    }\n" +
                            "  ]\n" +
                            "}")
                    .auth()
                    .with(httpBasic(SERVER_USER, SERVER_PASSWORD))
                .when()
                    .post("/orders")
                .then()
                    .status(HttpStatus.OK)
                    .body("beforeDiscountTotal", equalTo(8.0f))
                    .body("discount", equalTo(0))
                    .body("total", equalTo(8.0f))
                .extract()
                    .path("orderId");

        Order order = orderRepository.getOne(orderId.longValue());

        List<OrderItem> items = order.getItems();
        assertEquals(1, items.size());

        OrderItem orderItem = items.get(0);
        assertEquals("latte", orderItem.getProductName());
        assertEquals(BigDecimal.valueOf(2), orderItem.getAmount());
        assertEquals(latte.getPrice(), orderItem.getPrice());
    }
}