package tech.jborn.shopify;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.transaction.Transactional;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@Transactional
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
@ContextConfiguration(initializers = AbstractTest.Initializer.class)
public class AbstractTest {
    protected static final String SERVER_USER = "user";
    protected static final String SERVER_PASSWORD = "password";

    @Autowired
    protected MockMvc mockMvc;


    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        private PostgreSQLContainer postgres = new PostgreSQLContainer() {{
            withDatabaseName("shopify");
            withUsername("shopify");
            withPassword("shopify");
        }};

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            postgres.start();

            System.setProperty("SERVER_USER", SERVER_USER);
            System.setProperty("SERVER_PASSWORD", SERVER_PASSWORD);
            System.setProperty("POSTGRES_URL", postgres.getJdbcUrl());
            System.setProperty("POSTGRES_USER", postgres.getUsername());
            System.setProperty("POSTGRES_PASSWORD", postgres.getPassword());
        }
    }
}